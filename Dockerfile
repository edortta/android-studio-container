FROM ubuntu:22.04

RUN dpkg --add-architecture i386
RUN apt-get update

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y xorg

RUN apt-get install -y curl unzip

RUN curl 'https://dl.google.com/dl/android/studio/ide-zips/4.0.0.16/android-studio-ide-193.6514223-linux.tar.gz' > /studio.tar.gz && \
  tar -zxvf studio.tar.gz && rm /studio.tar.gz

RUN apt-get install -y libz1 libncurses5 libbz2-1.0:i386 libstdc++6 libbz2-1.0 lib32stdc++6 lib32z1
RUN apt-get install -y vim ant default-jdk

# Clean up
RUN apt-get clean
RUN apt-get purge

RUN useradd -ms /bin/bash user

ENV DISPLAY :0
USER user
ENV USER_HOME_DIR /home/user