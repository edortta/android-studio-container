# Android Studio Container


## TL;DR
```bash
git clone git@gitlab.com:edortta/android-studio-container.git android-33
cd android-33
./run.sh
```

## The history

These days, we needed an android studio that could be ready to use on different computers.
That led me to create this project that allows that to happen.

Since it's one of those projects designed to be used on a server (which is usually more powerful), you need an X11 server installed on your local machine.

## How to use

Let's say we want to build a new image with android-33 (the default), so you can just clone the project with an appropriate name to remember. After that, just run. The first time will waste some time, but once inside you can choose which SDK you need to install and so on.

The idea here is that you can clone this project as many times as you need. For example, having different SDKs per project. But that's up to you.

