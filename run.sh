#!/usr/bin/env bash

IMAGE=$(basename `pwd`)

cc=`docker image ls | grep $IMAGE | wc -l`
if [ $cc -eq 0 ]; then
  docker build -t $IMAGE . && cc=1
fi

if [ $cc -ne 0 ]; then
  docker run -it --net=host --env="DISPLAY" -v /tmp/.X11-unix -v "$HOME/.Xauthority:/home/user/.Xauthority:rw" -v"$(pwd)/user:/home/user" $(basename `pwd`) /android-studio/bin/studio.sh
fi